import React from 'react'
import ContactUsPage from '../components/ContactUsPage'
import {receiveEnquiryTypes} from '../actions/global'
import {bindActionCreators} from 'redux'
import {connect} from 'react-redux'

class ContactUsPageContainer extends React.Component {
  componentWillMount() {
    this.props.receiveEnquiryTypes()
  }
  
  render() {
    return <ContactUsPage {...this.props}/>
  }
}

const mapStateToProps = (state) => ({
  enquiryTypes: state.global.enquiryTypes
})

const mapDispatchToProps = dispatch => bindActionCreators({receiveEnquiryTypes}, dispatch)

export default connect(mapStateToProps, mapDispatchToProps)(ContactUsPageContainer)
