import React from 'react'
import {connect} from 'react-redux'
import {bindActionCreators} from 'redux'
import * as suggestionsActions from '../actions/suggestions'
import * as servicesActions from '../actions/services'
import {openModalWindow} from '../actions/global'
import ServicePage from '../components/ServicePage'

class ServicePageContainer extends React.Component {
  componentWillMount() {
    this.props.receiveFeaturedCompanies()
    this.props.receivePeopleMayKnow()
    this.props.receiveFeaturedProducts()
    this.props.receiveServices()
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.services.error !== this.props.services.error && !!nextProps.services.error)
      this.props.openModalWindow('Error', nextProps.services.error.description)
  }

  render() {
    return <ServicePage {...this.props}/>
  }
}

const mapStateToProps = ({suggestions, services}) => {
  return {
    featuredCompanies: suggestions.featuredCompanies,
    peopleMayKnow: suggestions.peopleMayKnow,
    featuredProducts: suggestions.featuredProducts,
    services: services
  }
}

const mapDispatchToProps = dispatch => bindActionCreators({
  receiveFeaturedCompanies: suggestionsActions.receiveFeaturedCompanies,
  receivePeopleMayKnow: suggestionsActions.receivePeopleMayKnow,
  receiveFeaturedProducts: suggestionsActions.receiveFeaturedProducts,
  receiveServices: servicesActions.receiveServices,
  openModalWindow
}, dispatch)

export default connect(mapStateToProps, mapDispatchToProps)(ServicePageContainer)
