import React from 'react'
import LeftMenu from '../components/LeftMenu'
import {leftMenuItems} from '../constants/globalConstants'

class LeftMenuContainer extends React.Component {
  state = {
    items: leftMenuItems
  }
  
  render() {
    return <LeftMenu {...this.state} />
  }
}

export default LeftMenuContainer
