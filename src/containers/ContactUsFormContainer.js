import React from 'react'
import ContactUsForm from '../components/ContactUsForm'
import {connect} from 'react-redux'
import {getFormValues} from 'redux-form'
import {bindActionCreators} from 'redux'
import {getImage, deleteImage, openModalWindow} from '../actions/global'

class ContactUsFormContainer extends React.Component {
  state = {
    enquiryTypesOther: false,
    descriptionCount: 0
  }

  changeDescriptionCount = (e) => {
    this.setState({descriptionCount: e.target.value.length})
  }

  handleSubmit = values => {
    if (this.state.enquiryTypesOther)
      values.enquiry_type = values.otherEnquiryType
    else values.enquiry_type = values.enquiryTypes

    values['user_name'] = values.name

    values.department = 1
    console.log(values)
    let data = new FormData()
    for (let name in values) {
      data.append(name, values[name]);
    }
    fetch('http://504080.com/api/v1/support', {
      method: 'POST',
      body: data
    })
      .then(response => response.json())
      .then(data => {
        if (data.success)
          this.props.openModalWindow('Success', data.data.message)
        else
          this.props.openModalWindow('Error', data.error.message)
      })
      .catch(error => console.log('fail'))
  }

  checkEnquiryTypes = e => this.setState({enquiryTypesOther: e.target.value === 'Other'})

  render() {
    return (
      <ContactUsForm
        onSubmit={this.handleSubmit}
        checkEnquiryTypes={this.checkEnquiryTypes}
        changeDesctiptionCount={this.changeDescriptionCount}
        {...this.props}
        {...this.state}
      />
    )
  }
}

const mapStateToProps = (state) => ({
  values: getFormValues('contactUsForm')(state),
  photoError: state.global.photoError,
  photoSrc: state.global.photoSrc
})

const mapDispatchToProps = dispatch => bindActionCreators({
  handleFile: getImage,
  deleteImage,
  openModalWindow
}, dispatch)

export default connect(mapStateToProps, mapDispatchToProps)(ContactUsFormContainer)

