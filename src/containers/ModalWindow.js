import ModalWindow from '../components/ModalWindow'
import {closeModalWindow} from '../actions/global'
import {bindActionCreators} from 'redux'
import {connect} from 'react-redux'


const mapStateToProps = (state) => ({
  modalWindowClosedClass: state.global.modalWindowClosedClass,
  modalWindowTitle: state.global.modalWindowTitle,
  modalWindowText: state.global.modalWindowText
})

const mapDispatchToProps = dispatch => bindActionCreators({closeModalWindow}, dispatch)

export default connect(mapStateToProps, mapDispatchToProps)(ModalWindow)

