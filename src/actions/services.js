import createReducer from '../createReducer'

const REQUEST_SERVICES = 'REQUEST_SERVICES'
const SUCCESS_RECEIVED_SERVICES = 'SUCCESS_RECEIVED_SERVICES'
const FAILURE_RECEIVED_SERVICES = 'FAILURE_RECEIVED_SERVICES'

const requestServices = () => ({type: REQUEST_SERVICES})

export const receiveServices = () => dispatch => {
  dispatch(requestServices())
  let headers = new Headers()
  headers.append('Content-Type', 'application/json')
  headers.append('Authorization', '7d4d0222a0c0be7e3e92ea460bf3acd52dc955aa')
  fetch('http://504080.com/api/v1/services/categories', {
    method: 'GET',
    headers
  })
    .then(response => response.json())
    .then(data => {
      if (data.success)
        return dispatch(successReceivedServices(data.data))

      return dispatch(failureReceivedServices(data.error))
    })
    .catch(error => dispatch(failureReceivedServices(error)))
}

const successReceivedServices = (data) => ({
  type: SUCCESS_RECEIVED_SERVICES,
  data
})

const failureReceivedServices = (error) => ({
  type: FAILURE_RECEIVED_SERVICES,
  error
})

const initialState = {
  data: [],
  isFetching: true,
  error: null
}

export const services = createReducer(initialState, {
  [REQUEST_SERVICES]: (state, action) => ({isFetching: true, error: null}),
  [SUCCESS_RECEIVED_SERVICES]: (state, {data}) => ({
    data,
    isFetching: false
  }),
  [FAILURE_RECEIVED_SERVICES]: (state, {error}) => ({
    error,
    isFetching: false
  })
})
