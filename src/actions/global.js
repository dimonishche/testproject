import createReducer from '../createReducer'
import {change} from 'redux-form'

const OPEN_MODAL_WINDOW='OPEN_MODAL_WINDOW'
const CLOSE_MODAL_WINDOW_ANIMATION='CLOSE_MODAL_WINDOW_ANIMATION'
const CLOSE_MODAL_WINDOW='CLOSE_MODAL_WINDOW'
const SUCCESS_RECEIVED_ENQUIRY_TYPES='SUCCESS_RECEIVED_ENQUIRY_TYPES'
const SET_PHOTO_ERROR='SET_PHOTO_ERROR'
const SET_PHOTO_SRC='SET_PHOTO_SRC'

export const openModalWindow = (modalWindowTitle, modalWindowText) => ({
  type: OPEN_MODAL_WINDOW, 
  modalWindowClosedClass: '',
  modalWindowTitle,
  modalWindowText
})

const closeModalWindowAnimation = () => ({
  type: CLOSE_MODAL_WINDOW_ANIMATION, 
  modalWindowClosedClass: 'modalBlockHiddenAnimation'
})

export const closeModalWindow = () => dispatch => {
  dispatch(closeModalWindowAnimation())
  const modalState = {
    type: CLOSE_MODAL_WINDOW,
    modalWindowClosedClass: 'modalBlockHidden'
  }
  setTimeout(() => dispatch(modalState), 1000)
}

export const receiveEnquiryTypes = () => dispatch => {
  fetch('http://504080.com/api/v1/directories/enquiry-types')
    .then(response => response.json())
    .then(data => {
      if (data.success)
        return dispatch(successReceivedEnquiryTypes(data.data))
    })
}

const successReceivedEnquiryTypes = enquiryTypes => ({
  type: SUCCESS_RECEIVED_ENQUIRY_TYPES,
  enquiryTypes
})

export const getImage = (e, file) => dispatch => {
  dispatch(setPhotoError(''))

  const fr = new FileReader()
  fr.onload = () => {
    const img = new Image()
    img.src = fr.result
    dispatch(setPhotoSrc(fr.result))
    img.onload = () => {
      if (img.width > 300 || img.height > 300) {
        dispatch(setPhotoSrc(''))
        dispatch(setPhotoError('The photo does not meet the requirements'))
      }
    }
  }
  fr.readAsDataURL(file)

  if (file.size > 5000000) {
    dispatch(setPhotoSrc(''))
    dispatch(setPhotoError('The photo does not meet the requirements'))
  }
}

const setPhotoError = photoError => ({type: SET_PHOTO_ERROR, photoError})

const setPhotoSrc = photoSrc => ({type: SET_PHOTO_SRC, photoSrc})

export const deleteImage = (e) => dispatch => {
  e.preventDefault()
  dispatch(change('ContactUsForm', 'photo', ''))
  dispatch(setPhotoSrc(''))
}

const initialState = {
  modalWindowClosedClass: 'modalBlockHidden',
  modalWindowTitle: '',
  modalWindowText: '',
  enquiryTypes: [],
  photoError: '',
  photoSrc: ''
}

export const global = createReducer(initialState, {
  [OPEN_MODAL_WINDOW]: (state, {modalWindowClosedClass, modalWindowTitle, modalWindowText}) => ({
    modalWindowClosedClass,
    modalWindowTitle, 
    modalWindowText
  }),
  [CLOSE_MODAL_WINDOW_ANIMATION]: (state, {modalWindowClosedClass}) => ({modalWindowClosedClass}),
  [CLOSE_MODAL_WINDOW]: (state, {modalWindowClosedClass}) => ({modalWindowClosedClass}),
  [SUCCESS_RECEIVED_ENQUIRY_TYPES]: (state, {enquiryTypes}) => ({enquiryTypes}),
  [SET_PHOTO_ERROR]: (state, {photoError}) => ({photoError}),
  [SET_PHOTO_SRC]: (state, {photoSrc}) => ({photoSrc})
})


