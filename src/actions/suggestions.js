import createReducer from '../createReducer'
import {featuredCompanies, peopleMayKnow, featuredProduct} from '../constants/globalConstants'

const REQUEST_FEATURED_COMPANIES = 'REQUEST_FEATURED_COMPANIES'
const SUCCESS_RECEIVED_FEATURED_COMPANIES = 'SUCCESS_RECEIVED_FEATURED_COMPANIES'
const REQUEST_PEOPLE_MAY_KNOW = 'REQUEST_PEOPLE_MAY_KNOW'
const SUCCESS_RECEIVED_PEOPLE_MAY_KNOW = 'SUCCESS_RECEIVED_PEOPLE_MAY_KNOW'
const REQUEST_FEATURED_PRODUCTS = 'REQUEST_FEATURED_PRODUCTS'
const SUCCESS_RECEIVED_FEATURED_PRODUCTS = 'SUCCESS_RECEIVED_FEATURED_PRODUCTS'

const requestFeaturedCompanies = () => ({type: REQUEST_FEATURED_COMPANIES})
const requestPeopleMayKnow = () => ({type: REQUEST_PEOPLE_MAY_KNOW})
const requestFeaturedProducts = () => ({type: REQUEST_FEATURED_PRODUCTS})

export const receiveFeaturedCompanies = () => dispatch => {
  dispatch(requestFeaturedCompanies())
  const companies = featuredCompanies
  return dispatch(successReceivedFeaturedCompanies(companies))
}

export const receivePeopleMayKnow = () => dispatch => {
  dispatch(requestPeopleMayKnow())
  const people = peopleMayKnow
  return dispatch(successReceivedPeopleMayKnow(people))
}

export const receiveFeaturedProducts = () => dispatch => {
  dispatch(requestFeaturedProducts())
  const products = featuredProduct
  return dispatch(successReceivedFeaturedProducts(products))
}

const successReceivedFeaturedCompanies = featuredCompanies => ({
  type: SUCCESS_RECEIVED_FEATURED_COMPANIES,
  featuredCompanies
})

const successReceivedPeopleMayKnow = peopleMayKnow => ({
  type: SUCCESS_RECEIVED_PEOPLE_MAY_KNOW,
  peopleMayKnow
})

const successReceivedFeaturedProducts = featuredProducts => ({
  type: SUCCESS_RECEIVED_FEATURED_PRODUCTS,
  featuredProducts
})

const initialState = {
  featuredCompanies: [],
  isFetchingCompanies: true,
  peopleMayKnow: [],
  isFetchingPeoples: true,
  featuredProducts: [],
  isFetchingProducts: true
}

export const suggestions = createReducer(initialState, {
  [REQUEST_FEATURED_COMPANIES]: (state, action) => ({isFetchingCompanies: true}),
  [REQUEST_PEOPLE_MAY_KNOW]: (state, action) => ({isFetchingPeoples: true}),
  [REQUEST_FEATURED_PRODUCTS]: (state, action) => ({isFetchingProducts: true}),
  [SUCCESS_RECEIVED_FEATURED_COMPANIES]: (state, {featuredCompanies}) => ({
    featuredCompanies: featuredCompanies,
    isFetchingCompanies: false
  }),
  [SUCCESS_RECEIVED_PEOPLE_MAY_KNOW]: (state, {peopleMayKnow}) => ({
    peopleMayKnow,
    isFetchingPeople: false
  }),
  [SUCCESS_RECEIVED_FEATURED_PRODUCTS]: (state, {featuredProducts}) => ({
    featuredProducts,
    isFetchingProducts: false
  })
})