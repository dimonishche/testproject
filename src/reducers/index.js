import {combineReducers} from 'redux'
import {suggestions} from '../actions/suggestions'
import {services} from '../actions/services'
import {global} from '../actions/global'
import {reducer as formReducer } from 'redux-form'

const rootReducer = combineReducers({
  suggestions,
  services,
  global,
  form: formReducer
})

export default rootReducer