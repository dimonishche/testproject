import React from 'react'
import './ServiceDirectory.css'

const ServiceDirectory = ({services}) => {

  return (
    <div className='serviceDirectory'>
      <div className='serviceDirectoryHead'>
        <h2 className='serviceDirectoryTitle'>Service Directory</h2>
        <button className='buttonStyle addNewService'>Add New Service</button>
      </div>
      <div className="serviceDirectoryContent">
        {services.data.map(item =>
          <div key={item.id} className='serviceItem'>
            <div className="topBlock">
              <img src={item.icon} alt={item.title}/>
            </div>
            <p className='serviceName'>{item.title}</p>
          </div>
        )}
      </div>
    </div>
  )
}
export default ServiceDirectory
