import React from 'react'
import ContactUsFormContainer from '../../containers/ContactUsFormContainer'
import About from '../About'
import Footer from '../Footer'
import './ContactUsPage.css'
import ModalWindow from '../../containers/ModalWindow'

const ContactUsPage = ({enquiryTypes}) => {
  return (
    <div className='contactUs'>
      <ModalWindow />
      <div className='backgrounds'>
          <img className='background' src={require('../../images/backgroundTop.png')} alt=""/>
          <img className='background' src={require('../../images/backgroundBottom.png')} alt=""/>
      </div>
      <div className='contactUsHead'>
        <div className='headTop'>
          <img className='logo' src={require('../../images/Logo.png')} alt='Logo'/>
          <button className='buttonStyle buttonLogIn'>Log In Now</button>
        </div>
        <div className='headBottom'>
          <h1 className='title'>Home of Dentistry</h1>
          <p className='titleDescription'>Denteez was created by dentists for dentistry in order to
            make the life of everyone involved in dentistry easier.</p>
        </div>
      </div>
      <div className='contactUsFormBlock'>
        <ContactUsFormContainer enquiryTypes={enquiryTypes}/>
      </div>
      <About />
      <Footer />
    </div>
  )
}

export default ContactUsPage
