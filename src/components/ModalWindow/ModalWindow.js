import React from 'react'
import './ModalWindow.css'

const ModalWindow = ({modalWindowClosedClass, closeModalWindow, modalWindowTitle, modalWindowText}) => {
  return (
    <div className={'modalBlock ' + modalWindowClosedClass}>
      <div className='modalWindow'>
        <div className='modalHeader'>
          <h3 className='modalTitle'>{modalWindowTitle}</h3>
          <span className='closeModal' onClick={closeModalWindow}>Close</span>
        </div>
        <div className='modalContent'>
          <p className='modalText'>{modalWindowText}</p>
        </div>
        <div className='modalAction'>
          <button className='modalButton' onClick={closeModalWindow}>Ok</button>
        </div>
      </div>
    </div>
  )
}

export default ModalWindow