import React from 'react'
import './Suggestions.css'

const Suggestions = props => {
  const {title, seeAllLink, items, itemTitleType, activeHandler, handlerTitle, firstDescClass, secondDescClass, handlerClass} = props
  return (
    <div className='suggestion'>
      {!items.length ? null : (
        <div>
          <div className='sugHead'>
            <h5>{title}</h5>
            <a className='seeAllLink' href={seeAllLink}>See All</a>
          </div>
          <div className='sugItems'>
            {items.map((item, index) =>
              <div key={index} className={itemTitleType === 1 ? 'sugItem sugItemType1' : 'sugItem'}>
                {itemTitleType === 1 ? <h4 className='sugItemTitle titleType1'>{item.title}</h4> : null}
                <img src={item.photo} alt={item.title}/>
                <div className='sugItemData'>
                  {itemTitleType === 2 ? <h4 className='sugItemTitle'>{item.title}</h4> : null}
                  {!!item.firstDescription ? <p className={'sugItemDescription ' + (firstDescClass || '')}>{item.firstDescription}</p> : null}
                  {!!item.secondDescription ? <p className={'sugItemDescription ' + (secondDescClass || '')}>{item.secondDescription}</p> : null}
                  {activeHandler ? <p className={'itemHandler ' + (handlerClass || '')}>{handlerTitle}</p> : null}
                </div>
              </div>
            )}
          </div>
        </div>
      )}
    </div>
  )
}

export default Suggestions
