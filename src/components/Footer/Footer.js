import React from 'react'
import './Footer.css'

const Footer = () => {
  return (
    <div className='footer'>
      <p className='copyright'>Denteez Copyright 2015</p>
      <div className='bottomLinks'>
        <a href='/'>Support</a>
        <a href='/'>Privacy Policy</a>
        <a href='/'>Terms of use</a>
      </div>
    </div>
  )
}

export default Footer
