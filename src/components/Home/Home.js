import React from 'react'
import './Home.css'

const Home = () => {
  return (
    <div className='links'>
      <a href='/services'>Services</a>
      <br/>
      <a href='/contact_us'>Contact Us</a>
    </div>
  )
}

export default Home
