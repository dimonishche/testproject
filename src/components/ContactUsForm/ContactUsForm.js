import React from 'react'
import './ContactUsForm.css'
import {Field, reduxForm} from 'redux-form'

const ContactUsForm = (props) => {
  const {handleSubmit, enquiryTypes, enquiryTypesOther, checkEnquiryTypes, handleFile, photoError, changeDesctiptionCount, descriptionCount, photoSrc, deleteImage} = props
  console.log(photoError)
  return (
    <form onSubmit={handleSubmit}>
      <div className='inputsBlock'>
        <p className='formRule'>Fields marked “*” are required</p>
        <div className='formRow'>
          <label htmlFor='enquiryTypes'>Enquiry Type *</label>
          <Field onChange={checkEnquiryTypes} className='inputStyle selectStyle' name='enquiryTypes' id='enquiryTypes'
                 component='select'>
            {enquiryTypes.map((item, index) => <option key={index} value={item.name}>{item.name}</option>)}
          </Field>
          <Field className={`inputStyle ${enquiryTypesOther ? '' : 'hidden'}`} id='otherEnquiryType'
                 name='otherEnquiryType' component={renderField} type='text'
                 label='Other'/>
        </div>
        <div className='halfWidth nameInput formRow'>
          <label htmlFor='name'>Name *</label>
          <Field className='inputStyle' id='name' name='name' component={renderField} type='text'
                 placeholder='Dantist' label='Dentist'/>
        </div>
        <div className='halfWidth formRow'>
          <label htmlFor='email'>Email *</label>
          <Field className='inputStyle' id='email' name='email' component={renderField} type='input'
                 label='rachelm@gmail.com'/>
        </div>
        <div className='formRow'>
          <label htmlFor='subject'>Subject *</label>
          <Field className='inputStyle' id='subject' name='subject' component={renderField} type='text'/>
        </div>
        <div className='formRow'>
          <label htmlFor='description'>Description *</label><label >({descriptionCount}/1000)</label>
          <Field className='inputStyle' onChange={changeDesctiptionCount} id='description' name='description' component={renderField} textarea={true}/>
        </div>
        <div className='formRow'>
          <label htmlFor='photo' className='loadPhotoLabel'>
            <img onClick={deleteImage} className={!photoSrc ? 'hidden deleteImage' : 'deleteImage'} src={require('../../images/deleteImage.png')} alt=""/>
            <span className='addPhotoTitle'>
              Add photo
              <span className='addPhotoRules'>Minimum size of 300x300 jpeg ipg png 5 MB</span>
            </span>
            <span className='photoError'>
              {photoError}
            </span>
            <img className={!!photoError ? 'hidden loadedImage' : 'loadedImage'} src={photoSrc} alt=""/>
          </label>
          <Field name='photo' id='photo' className='inputStyle' component={FileInput} onChange={handleFile}/>
        </div>
      </div>
      <button type='submit' className='buttonStyle submitForm'>Submit</button>
    </form>
  )
}

const renderField = ({input, id, className, label, type, textarea, meta: {touched, error}}) => {
  const textareaType = <textarea {...input}
    placeholder={label}
    type={type}
    className={`${className} ${touched && error ? 'errorField' : ''}`}
    id={id}
    maxLength='1000'
  />
  const inputType = <input {...input}
    placeholder={label}
    type={type}
    className={`${className} ${touched && error ? 'errorField' : ''}`}
    id={id}
  />
  return (
    <div className='field'>
      {textarea ? textareaType : inputType}
      {touched &&
      error ?
        <span className='inputError'>
          {error}
        </span> : <span className='inputError'></span>}
    </div>
  )
}

export const FileInput = ({input, resetKey, id, className, photoError, meta: {touched, error}}) => {
  const {value, ...inputProps} = input

  const handleChange = (e) => {
    input.onChange(e.target.files[0])
  }

  return <input {...inputProps} id={id} className={className} key={resetKey} type="file" onChange={handleChange}
                             onBlur={() => {}} accept='image/jpg, image/jpeg, image/png'/>
}

export const validate = values => {
  const errors = {};
  if (!values.name)
    errors.name = 'The field is required'
  if (!values.email)
    errors.email = 'The field is required'
  else if (!/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(values.email)) {
    errors.email = 'Invalid email address'
  }
  if (!values.subject)
    errors.subject = 'The field is required'
  if (!values.description)
    errors.description = 'The field is required'
  if (values.enquiryTypes === 'Other' && !values.otherEnquiryType)
    errors.otherEnquiryType = 'The field is required'
  
  return errors
}

  export default reduxForm({form: 'contactUsForm', validate})(ContactUsForm)
