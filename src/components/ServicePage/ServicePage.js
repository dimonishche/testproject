import React from 'react'
import Header from '../Header'
import LeftMenuContainer from '../../containers/LeftMenu'
import Advertisement from '../Advertisement'
import Suggestions from '../Suggestions'
import ServiceDirectory from '../ServiceDirectory'
import ModalWindowContainer from '../../containers/ModalWindow'
import './ServicePage.css'

class ServicePage extends React.Component {
  render() {
    const {featuredCompanies, peopleMayKnow, featuredProducts, services} = this.props

    return (
      <div>
        <ModalWindowContainer />
        <Header />
        <div className='servicePageContent'>
          <aside className='sidebar'>
            <LeftMenuContainer />
            <Advertisement
              imageUrl={require('../../images/Advertisement1.png')}
              adBy='Ads By Denteez.com'
            />
            <div className='divider'></div>
            <Suggestions
              title='Featured Companies'
              seeAllLink='/'
              itemTitleType={2}
              items={featuredCompanies}
              activeHandler={true}
              handlerTitle='Follow Now'
            />
            <div className='divider'></div>
            <p className='copyright'>Denteez Copyright 2015</p>
            <div className='bottomLinks'>
              <a className='termOfUse' href='/'>Terms of use</a>
              <a href='/'>Privacy Policy</a>
            </div>
          </aside>
          <main className='services'>
            <ServiceDirectory services={services}/>
          </main>
          <aside className='sidebar'>
            <Suggestions
              title='People you may know'
              seeAllLink='/'
              itemTitleType={1}
              items={peopleMayKnow}
              activeHandler={true}
              handlerTitle='Add Friend'
              handlerClass='addFriend'
            />
            <div className='divider'></div>
            <Suggestions
              title='Featured Products'
              seeAllLink='/'
              itemTitleType={1}
              items={featuredProducts}
              activeHandler={false}
              firstDescClass={'productsFirstDescription'}
              secondDescClass={'productsSecondDescription'}
            />
            <div className='divider'></div>
            <Advertisement
              imageUrl={require('../../images/Advertisement2.png')}
              adBy='Ads By Denteez.com'
            />
          </aside>
        </div>
      </div>
    )
  }
}

export default ServicePage
