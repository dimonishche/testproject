import React from 'react'
import './Advertisement.css'

const Advertisement = ({imageUrl, adBy}) => {
  return (
    <div>
      <h5 className='adTitle'>Advertisement</h5>
      <div className='adImage'>
        <img src={imageUrl} alt='title'/>
      </div>
      <p className='adBy'>{adBy}</p>
    </div>
  )
}

export default Advertisement