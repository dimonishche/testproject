import React from 'react'
import {Route} from 'react-router-dom'
import ServicePageContainer from '../../containers/ServicePage'
import ContactUsPageContainer from '../../containers/ContactUsPageContainer'
import Home from '../Home'
import './App.css'

class App extends React.Component {
  render() {
    return (
      <div>
        <Route exact path='/' component={Home}/>
        <Route path='/services' component={ServicePageContainer}/>
        <Route path='/contact_us' component={ContactUsPageContainer}/>
      </div>
    )
  }
}

export default App

