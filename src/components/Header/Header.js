import React from 'react'
import './Header.css'

class Header extends React.Component {
  render() {
    return (
      <div className='navbar'>
        <div className='logo'>
          <img src={require('../../images/Logo.png')} alt='Logo'/>
        </div>
        <div className='searchBar'>
          <input className='searchField' type='text' placeholder='Company Name'/>
        </div>
        <div className='rightBar'>
          <img className='chatLogo' src={require('../../images/Chat.png')} alt=''/>
          <img className='notificationLogo' src={require('../../images/Notifications.png')} alt=''/>
          <div className='loggedUser'>
            <img src={require('../../images/UserPhoto.png')} alt=''/>
            <span className='userName'>Maximillian Beekeeper</span>
          </div>
        </div>
      </div>
    )
  }
}

export default Header
