import React from 'react'
import './LeftMenu.css'

const LeftMenu = ({items}) => {
  return (
    <div className='leftMenu'>
      {items.map((item, id) =>
        <li key={id}>
          <div className='menuIcon'>
            <img src={item.icon} alt='item.title'/>
          </div>
          {item.title}
        </li>
      )}
    </div>
  )
}

export default LeftMenu
