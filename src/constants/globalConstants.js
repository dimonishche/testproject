export const leftMenuItems = [
  {
    title: 'Feed',
    icon: require('../images/Feed.png')
  },
  {
    title: 'Ask a Colleague',
    icon: require('../images/Ask.png')
  },
  {
    title: 'Companies',
    icon: require('../images/Companies.png')
  },
  {
    title: 'Service Directory',
    icon: require('../images/Services.png')
  }
]

export const featuredCompanies = [
  {
    title: 'Company Name',
    firstDescription: 'Manufacturer',
    secondDescription: 'Belgrade, Serbia',
    photo: require('../images/Suggestion1.1.png')
  },
  {
    title: 'Company Name',
    firstDescription: 'Service Provider',
    secondDescription: 'New York, USA',
    photo: require('../images/Suggestion1.2.png')
  },
  {
    title: 'Company Name',
    firstDescription: 'Supplier',
    secondDescription: 'London, England',
    photo: require('../images/Suggestion1.3.png')
  }
]

export const peopleMayKnow = [
  {
    title: 'Dennis Adams',
    firstDescription: 'Dentist (Practice Owner)',
    secondDescription: 'London, England',
    photo: require('../images/Suggestion2.3.png')
  },
  {
    title: 'Mary Carpenter',
    firstDescription: 'Dentist (Practice Owner)',
    secondDescription: 'Belgrade, Serbia',
    photo: require('../images/Suggestion2.2.png')
  },
  {
    title: 'Danielle Salazar',
    firstDescription: 'Dentist (Practice Owner)',
    secondDescription: 'Paris, France',
    photo: require('../images/Suggestion2.1.png')
  }
]

export const featuredProduct = [
  {
    title: 'Product Name',
    firstDescription: 'Product Short Description.',
    secondDescription: 'The quick brown fox jumps over the lazy dog.',
    photo: require('../images/Suggestion3.1.png')
  },
  {
    title: 'Product Name',
    firstDescription: 'Product Short Description.',
    secondDescription: 'The quick brown fox jumps over the lazy dog.',
    photo: require('../images/Suggestion3.2.png')
  }
]
